#!/usr/bin/env python
import sys, re

# color escape sequences
color_red="\x1b[31;01m"
color_green="\x1b[00;32m"
color_blue="\x1b[34;01m"
color_normal="\x1b[00m"

def nxos_help_string(args):
    # display online help (if asked for) for NX-OS' "source" command 
    # either in config mode or at the exec prompt, if users type "source <script> ?" then help_string appears
	# CREDIT: https://github.com/datacenter
    nb_args = len(args)
    if nb_args > 1:
        help_string = color_green + "Version of show ip int br, including subnet masks. No arguments required." + color_normal
        m = re.match('__cli_script.*help', args[1])
        if m:
                # user entered "source ?" with no parameters: display script help
                if args[1] == "__cli_script_help":
                    print help_string
                    exit(0)
                # argument help
                argv = args[2:]
                # dont count last arg if it was partial help (no-space-question-mark)
                if args[1] == "__cli_script_args_help_partial":
                    argv = argv[:-1]
                nb_args = len(argv)
                # only file name provided: use man-page style help
                print "__man_page"
                print help_string
                exit(0)

def calcDottedNetmask(mask):
    #Converts a CIDR number into a dotted netmask - i.e. 24 returns 255.255.255.0
	#CREDIT: http://stackoverflow.com/questions/819355/how-can-i-check-if-an-ip-is-in-a-network-in-python
    bits = 0
    for i in xrange(32-mask,32):
        bits |= (1 << i)
    return "%d.%d.%d.%d" % ((bits & 0xff000000) >> 24, (bits & 0xff0000) >> 16, (bits & 0xff00) >> 8 , (bits & 0xff))

#main

#Return the help if nexus called it (i.e. source ?)
nxos_help_string(sys.argv)

#Grab the interfaces as structured data
interfaces = clid("show ip interface")

#Count the interfaces
intcount = 0
for key in interfaces.keys():
  if 'intf-name' in key:
    intcount += 1

#Define a fixed width table
table = '{:<25}{:<15}{:<5}{:<16}{:<10}{:<10}{:<10}'

#Print table headers
print table.format('Name','IP','CIDR','Mask','Admin','Link','Protocol')

#Loop the interfaces, grab the relevant bits and print each row
for x in range(1,intcount+1):
  name = interfaces['TABLE_intf/intf-name/'+str(x)]
  ip = interfaces['TABLE_intf/prefix/'+str(x)]
  cidr = interfaces['TABLE_intf/masklen/'+str(x)]
  mask = calcDottedNetmask(int(cidr))
  admin = interfaces['TABLE_intf/admin-state/'+str(x)]
  link = interfaces['TABLE_intf/link-state/'+str(x)]
  protocol = interfaces['TABLE_intf/proto-state/'+str(x)]
  print table.format(name,ip,cidr,mask,admin,link,protocol)

