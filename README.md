# nexus-python
## list-interfaces.py
This scripts has a similar output to "show ip interface brief", but includes subnet masks.
Upload to bootflash:scripts/list-interfaces.py.
Invoke with "source list-interfaces.py".
You can also add a command alias:
```
conf t
  cli alias name listint source list_interfaces.py
```
This would allow the script to be invoked just by typing "listint".